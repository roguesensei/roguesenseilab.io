---
title: "About"
date: 2023-02-04T13:03:25Z
draft: false
---
# Hello my friends
The only thing greater than computers and code is a cup of tea. I drink tea and I code things.

My favourite language is C++; I also like plain old C, C# and Python.
# My Personal Projects
- [Wilson](https://gitlab.com/roguesensei/wilson.git) - My discord bot written in Python
- [CLogger](https://gitlab.com/roguesensei/clogger.git) - A tiny logging library written in C
- More to be published soon.

Consider inviting Wilson to your discord server by clicking [here](https://discordapp.com/api/oauth2/authorize?client_id=370210465672069122&permissions=8&scope=bot).

Thanks for stopping by, happy hacking!
