+++ 
draft = false
date = 2023-03-02T11:37:14Z
title = "CLogger"
description = "A small login library written in C used in my C/C++ projects"
slug = "" 
+++
CLogger (pronounced "C Logger") is a logging library written in C, and is the project I used to learn C. I use it in my C/C++ projects and tend to make drastic breaking changes to keep the project simple (and modify as I change the way I like writing C).

You can poke around the source code [here on my GitLab](https://gitlab.com/roguesensei/clogger), and you can read the API reference [here](https://rs-clogger.readthedocs.io/en/latest/).
# Features
- Simple functional logging functions for different log levels
- Logging to console or files
- Assertion/Expect functions
- Console colours :D
- Data structure for configurable logging environments (also useful for OOP approach in C++)
