+++ 
draft = false
date = 2024-03-23
title = "Wilson"
description = "My Discord bot written in Rust"
+++
Wilson is my discord bot written in Rust using the [serenity](https://github.com/serenity-rs/serenity) library paired with the [poise](https://github.com/serenity-rs/poise) framework. Succeeding the original [python](https://gitlab.com/roguesensei/wilson) version, the new Rust version of Wilson, while being a remake, aims to continue representing the philosophies of the original, maintaining the "free forever" FOSS approach to discord bot culture.

If you're interested in inviting Wilson to your server, click [here](https://discordapp.com/api/oauth2/authorize?client_id=370210465672069122&permissions=8&scope=bot). If you wish to poke around Wilson's source code, you can find it [here on my GitLab](https://gitlab.com/roguesensei/rusty-wilson).
# Features
- Moderation Commands (such as banning, timeouts etc.)
- General fun/info commands
- Pure slash commands
- Open source!!!
