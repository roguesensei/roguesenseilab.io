+++ 
draft = false
date = 2023-03-01
title = "Wilson (Original)"
description = "My Discord bot written in Python"
+++
Wilson is my discord bot written in Python using the [discord.py](https://github.com/Rapptz/discord.py) library. He is also my first major, and oldest, code project. He started as a small college project to learn programming and has since grown into a bot more representative of what I want to see more of in the discord bot space.

He has since been succeeded by the [rust](https://gitlab.com/roguesensei/rusty-wilson) version with a fresh coat of paint, but if you wish to poke around Wilson's original source code, you can find it [here on my GitLab](https://gitlab.com/roguesensei/wilson).
# Features
- Moderation Commands (such as banning, timeouts etc.)
- Welcome actions (Such as autorole)
- General fun/info commands
- Some slash commands
- Open source!!!
