+++ 
draft = true
date = 2023-03-04
title = "Wilson Development Retrospective"
+++
This article is retrospective on Wilson's development from 2017 to 2023 (hard to believe I've spent around 6 years on this project) so if you wish to hear my tale, grab a hot drink and scroll through my wall of text.
# Early Development
In a way, Wilson was the project that got me so enthusiastic about programming in the first place even with his shortcomings. The source code was contained in a single PY file on a USB drive. I wasn't taking advantage of [cogs](https://discordpy.readthedocs.io/en/stable/ext/commands/cogs.html) and I added commands using the excellent copy & paste design pattern.

I developed him alongside friends, who had bots of their own varying in functionality, and I took the route of developing a general purpose moderation bot (we even had a bot development server to showcase and test each other's bots). Many of the commands I developed back then still exist today including `|ban`, `|sweep`, `|f`, `|lenny` and of course, `|hello` (which I later added an easter egg to).

![hello_there](https://i.giphy.com/media/xTiIzJSKB4l7xTouE8/giphy.webp)

As development continued, I decided to find a way to get Wilson permanently online and release the first official update. As it turned out, I happened to have a Raspberry Pi 1 Model B+ (yes, the generation) and figured it would be the perfect vessel to accomplish this.
# The Golden Age
This update was the first documented update complete with a changelog (See [Update History](#update-history) for list of changes over the years).
# Update History
## Version 0.5 - The Pi Home Update (Released approximately Spring/Summer 2018)
Wilson now runs permanently on RogueSensei's Raspberry Pi, his new home! With that too, Wilson has some nifty new changes.
### New Features
- `release`: Like release details? Yes you do! You can now check Wilson's stats!
- `update`: This very command which you see before you; exciting!
- `flip`: Flip a coin! `V0.5.1`
- `f`: Everyone pay your respects. `V0.5.1`
- `react r m`: New and improved reactions! Type `|help react` `V0.5.2`
- `invite`: It's about time! `V0.5.2`
- `image`: Embed an image. `V0.5.3`
- `dice i`: Dice roll command `V0.5.4`
### Updated Features
- `sweep`: Due to upcoming Moderation features, `sweep` is now private.
- `sleep`: Kill is now Sleep again. Hey, I'm the pest bot, may as well play the part.
- `help`: Minor updates to existing help files to match changes.
- `react r`: 5 new reactions! `V0.5.1`
- `help`: Major update to help files to public commands `V0.5.4`
### Removed Features
- Independent reaction commands minus `fight m`. `|dance` is now `|react dance`.
### Fixes
- Wilson now runs slightly smoother and reboots on his own. `V0.5.3`
- The release timer has been rewritten and should be more accurate. `V0.5.5`
## Version 1.0 - The Core Update (Released April 2019)
Yay! Version 1.0! Completely re-written, new features, better than ever!
### Behind the Scenes
From a technical perspective, **Wilson's** code base has been completely re-written to be more efficient to improve performance and maintainability. Many of the commands you know and love are back and appear unchanged, however some unused features have also been removed.
### New Features
- `avatar`: See those sweet, sweet avatars close up!
- Moderator commands, at last! *Require user permissions*
- `kick`
- `ban`
*See the help of the moderator commands for more information on permissions of their use*
### Updated Features
- `sweep` is back and better than ever!
- `dupe` is now available for moderators. Use with care before I nerf it...
### Removed Features
- `quickmaffs`: Dead meme
- `evil`: Not really used...
- `invite` has been temporarily moved for stress testing **Wilson**
## Version 1.1 - The Core++ Update
With a re-write after a re-write comes a new re-write, it's Version 1.1!
### Introducing Core++
Now the `discord.py` API re-write has become *the* `discord.py`, **Wilson** has been re-written (for hopefully the last time in a while) once more! In addition, some new features following from the last update have been added, but most notable is the new sever-specific settings!
### Server Specific Settings
Wilson can now store some settings unique to your server, which includes:
- Leaderboards
- Default/Auto Role
- Server Logging
- Welcome Messages
- Swears and Custom Commands
You can opt in or out of these with `|optin` and `|optout` respectively and can view the settings for these with `|settings`
### New Features
- `avatar` command
- `whois` command
- `serverinfo` command
- `message` command
- `send` command
- Image APIs
### Image APIs, you say?
Yes, calls to image-based APIs, including tag-based searching (`imgur`), and for all you weebs, commands for the nekos.life API (`|help nekos`)
### Updated Features
- On the flipside to `|ban`, there is now `|unban`. Nifty.
### Removed Features
- The joke is dead now, **Wilson** no longer responds to pings of himself.
- **Wilson** no longer DMs people he has banned.
## Version 1.2 - The Redemption Update
Version 1.2 - The update following Wilson's ban; he's newer and shiner with a brand new database
```diff
+ New Database integration
+ Implemented waifu.im API (V1.2d)
- Removed |levels command
- Removed |rank command
- Temporarily disabled custom commands
- Temporarily disabled swears (may remove permanently)
- Removed commands implementing discontinued NSFW nekos.life API endpoints
```
## Version 2.0 - The FOSS Update
The fourth rewrite integrating `discord.py` 2.0 features including slash commands. This update also sees Wilson officially being licensed as Free Open Source Software (GNU GPL V3 LICENSE).
```
+ Added serverav command
+ Modififed whois/av command to use player server profile
+ mute/shun now uses discord's timeout feature
+ Slash commands
+ Support for plugins
- removed dupe command (this one won't be back)
```
