---
title: "Oh my god, a webby site"
date: 2023-02-04T13:16:20Z
draft: false
---
# A blog, you say?
This page may disappear one day, depending on what I decide to do with this website. For now, marvel at this excellent static page.
